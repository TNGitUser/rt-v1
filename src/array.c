/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 11:43:03 by lucmarti          #+#    #+#             */
/*   Updated: 2019/05/27 14:02:33 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

static void		objs_array_cpy(t_obj **dst, t_obj **src, size_t n)
{
	size_t	i;

	if (!dst || !src)
		exit(1);
	if (n == 0)
		return ;
	i = 0;
	while (i < n)
	{
		dst[i] = src[i];
		++i;
	}
	ft_memdel((void **)&src);
}

static t_obj	**objs_array_realloc(t_obj **objs, size_t nsize)
{
	t_obj **array;

	if (!objs || nsize == 0
			|| !(array = malloc(sizeof(t_obj *) * (nsize + 1))))
		exit(1);
	array[nsize] = 0;
	objs_array_cpy(array, objs, nsize);
	return (array);
}

void			objs_append(t_obj *obj, t_scene *scene)
{
	if (!obj || !scene || !(scene->objs))
		exit(1);
	scene->objs = objs_array_realloc(scene->objs, scene->objs_count + 1);
	scene->objs[scene->objs_count] = obj;
	scene->objs_count += 1;
}
