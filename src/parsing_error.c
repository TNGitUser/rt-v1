/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing_error.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 12:46:45 by lucmarti          #+#    #+#             */
/*   Updated: 2019/05/29 12:18:47 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void	parse_scene_error(void)
{
	ft_putstr_fd("Error, imbricated scenes\n", 2);
	ft_putstr_fd("Aborting !\n", 2);
	exit(1);
}

void	parse_obj_error(void)
{
	ft_putstr_fd("Error, imbricated objects\n", 2);
	ft_putstr_fd("Aborting !\n", 2);
	exit(1);
}

void	parse_action_error(void)
{
	ft_putstr_fd("Error, missing action, variable name, value", 2);
	ft_putstr_fd(" or bad formatting\n", 2);
	ft_putstr_fd("Aborting !\n", 2);
	exit(1);
}

void	parse_start_error(void)
{
	ft_putstr_fd("Error, missing start scene or bad formatting\nAborting !\n",
			2);
	exit(1);
}

void	parse_error(int pos, size_t linec, char *line)
{
	printf("Error on line %zu:%i\n---->`%s`\n", linec, pos, line);
	ft_memdel((void **)&line);
	printf("Aborting !\n");
	exit(1);
}
