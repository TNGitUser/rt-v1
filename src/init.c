/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 11:38:14 by lucmarti          #+#    #+#             */
/*   Updated: 2019/05/24 12:17:54 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void	scene_init(t_scene *scene)
{
	if (!scene)
		exit(1);
	obj_array_init(scene);
}

void	obj_array_init(t_scene *scene)
{
	t_obj	**array;

	array = NULL;
	if (!(array = malloc(sizeof(t_obj *))))
		exit(1);
	array[0] = NULL;
	scene->objs = array;
	scene->objs_count = 0;
}
