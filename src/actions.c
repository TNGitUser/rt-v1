/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   actions.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 14:33:32 by lucmarti          #+#    #+#             */
/*   Updated: 2019/05/29 11:44:30 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

static char	*get_data(char *data)
{
	char	*extracted_data;
	char	*start_data;
	char	*end_data;
	char	*tmp;

	if (!data || !(start_data = ft_strchr(data, '{'))
				|| !(end_data = ft_strrchr(data, '}')))
		return (NULL);
	++start_data;
	while (*start_data && ft_isspace(*start_data))
		++start_data;
	extracted_data = ft_strsub(start_data, 0, end_data - start_data - 1);
	tmp = extracted_data;
	extracted_data = ft_strdup(tmp);
	ft_memdel((void **)&tmp);
	if (!extracted_data)
		die("Error in get_data()(actions.c:25)\n");
	return (extracted_data);
}

/*
**	cam actions
**		- origin: set the position of the camera
**		- pov	: point of view
**		- up    : vector that start from the camera and point upward
**		- dir	: vector that give the direction of the camera
**		- right	: right vector fo the camera
*/

static void	exec_cam_actions(t_scene *scene, int action, char *line,
		t_state *state)
{
	char	*data;

	data = get_data(line);
	if (action == 0)
		exec_set_vector(&(scene->camera.pos), state, data);
	if (action == 1 || action == 2)
		exec_set_vector(action == 1 ? &(scene->camera.pov)
				: &(scene->camera.up), state, data);
	if (action == 3 || action == 4)
		exec_set_vector(action == 3 ? &(scene->camera.dir)
				: &(scene->camera.right), state, data);
	if (action == 5)
	{
		state->obj_end = 1;
		state->obj_action = 0;
		state->cur_action = -1;
	}
	ft_memdel((void **)&data);
	return ;
}

/*
**	obj actions
**		- name	: copy the name between `{}` in the line
**		- radius: set the radius to the radius indicated between `{}`
**		- length: set the length to the length between `{}`
**		- height: set the height ...
**		- color	: set the color  ... (rgb)
**		- origin: set the origin ... (vector3 (x,y,z))
*/

static void	exec_obj_actions(t_scene *scene, int action, char *line,
		t_state *state)
{
	char	*data;

	data = get_data(line);
	if (action == 4 && state->obj_end == 1)
	{
		state->obj_end = 0;
		state->cur_obj = obj_create("NULL", 0, 0, 0);
	}
	else if (action == 4)
		obj_set_color(ft_atoi_base(data + 2, 16), state->cur_obj);
	if (action == 0)
		obj_set_type(data, state->cur_obj);
	if (action == 1 || action == 2 || action == 3)
		exec_obj_set(state, data);
	if (action == 5 || action == 6)
		exec_set_vector(action == 5 ? &(state->cur_obj->pos)
				: &(state->cur_obj->rot), state, data);
	if (action == 7)
	{
		obj_verify_correct(state->cur_obj);
		state->obj_end = 1;
		state->obj_action = 0;
		objs_append(state->cur_obj, scene);
	}
	ft_memdel((void **)&data);
}

/*
**	main actions
**		- name	: copy the name between `{}` in the line
**		- camera: set the camera at specified coordinates
**		- render: set the length to the length between `{}`
**		- object: create new object and append it to the list then do obj action
*/

static void	exec_main_actions(t_scene *scene, int action, char *line)
{
	size_t	i;
	char	*data;

	i = 0;
	data = get_data(line);
	if (action == 1 || action == 3)
	{
		if (action == 1)
		{
			if (scene->name)
				ft_memdel((void **)&(scene->name));
			scene->name = ft_strdup(data);
		}
		if (action == 3)
		{
			scene->render.width = ft_atoi(data);
			while (data[i] != '\0' && data[i] != ' ')
				++i;
			scene->render.height = ft_atoi(data + i);
		}
	}
	ft_memdel((void **)&data);
}

/*
**	exec_action() will use the correct function depending of the situation
**	there is 4 and 6 actions (respectivly for the main and obj)
*/

void		exec_action(t_state *state, t_scene *scene, char *line)
{
	size_t	i;

	i = 0;
	if (state->cur_action < 0)
		return ;
	while (ft_isspace(line[i]))
		++i;
	if (state->obj_action == 1)
		exec_obj_actions(scene, state->cur_action, line + i, state);
	else if (state->obj_action == 2)
		exec_cam_actions(scene, state->cur_action, line + i, state);
	else
		exec_main_actions(scene, state->cur_action, line + i);
}
