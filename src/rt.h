/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 12:21:25 by lucmarti          #+#    #+#             */
/*   Updated: 2019/07/05 12:34:35 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RT_H
# define RT_H

# include "../libs/mlx/mlx.h"
# include "../libs/libft/libft.h"
# include "get_next_line.h"

# include <fcntl.h>
# include <stdlib.h>
# include <unistd.h>
# include <math.h>

# include "obj.h"

# include <stdio.h>

/*
**	PARSING STRUCTURE       ---------------------------------------------------
*/

typedef struct	s_state
{
	int			cur_action;
	int			parent_action;
	t_obj		*cur_obj;
	int			obj_end;
	int			obj_action;
	int			cur_line;
}				t_state;

/*
**	END OF PARSING STRUCTURE---------------------------------------------------
*/

typedef struct	s_pctr
{
	void		*adr;
	int			*data;
	int			size_l;
	int			bpp;
	int			endian;
}				t_pctr;

typedef struct	s_render
{
	void		*mlx_ptr;
	void		*mlx_win;
	t_pctr		pctr;
	int			width;
	int			height;
}				t_render;

typedef struct	s_camera
{
	t_vector3	pos;
	t_vector3	pov;
	t_vector3	up;
	t_vector3	dir;
	t_vector3	right;
}				t_camera;

typedef	struct	s_scene
{
	char		*name;
	t_obj		**objs;
	size_t		objs_count;
	t_render	render;
	t_camera	camera;
	t_viewplane	vp;
}				t_scene;

/*
**	actions.c
*/
void			exec_action(t_state *state, t_scene *scene, char *line);

/*
**	array.c
*/
void			objs_append(t_obj *obj, t_scene *scene);

/*
**	error.c
*/
void			die(char *str);

/*
**	file.c
*/
void			get_scene(char *cfile, t_scene *scene);

/*
**	init.c
*/
void			scene_init(t_scene *scene);
void			obj_array_init(t_scene *scene);

/*
**	misc.c
*/
int				ft_isspace(char c);

/*
**	obj_actions.c
*/
void			obj_verify_correct(t_obj *obj);
void			exec_set_vector(t_vector3 *vec, t_state *state, char *data);
void			exec_obj_set(t_state *state, char *data);

/*
**	parsing.c
*/
int				start_analysis(char *line, int is_start, t_state *state);

/*
**	parsing_error.c
*/
void			parse_scene_error(void);
void			parse_obj_error(void);
void			parse_action_error(void);
void			parse_start_error(void);
void			parse_error(int pos, size_t linec, char *line);

/*
**	rotation.c
*/
void			all_rot(t_vector3 *vec, t_vector3 *rot, int invert);

#endif
