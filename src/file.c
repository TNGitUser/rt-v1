/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 13:19:27 by lucmarti          #+#    #+#             */
/*   Updated: 2019/05/29 12:14:07 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

static int	check_line(char *line)
{
	int	i;

	i = 0;
	while (line[i])
	{
		if (line[i] == '#' && i == 0)
			break ;
		if (!(ft_isalnum(line[i]) || line[i] == '{' || line[i] == '}'
					|| line[i] == '_' || line[i] == ':' || ft_isspace(line[i])
					|| line[i] == '.' || line[i] == '-'))
			return (-i);
		++i;
	}
	return (1);
}

static int	line_is_commented(char *line)
{
	size_t	i;

	i = 0;
	while (line[i] && ((line[i] >= 48 && line[i] <= 59) || (line[i] == 123
					|| line[i] == 125 || line[i] == 35 || ft_isspace(line[i]))))
	{
		if (line[i] == 35)
			return (1);
		++i;
	}
	return (0);
}

static void	init_loop(t_state *state, t_scene *scene)
{
	state->cur_action = -1;
	state->obj_action = 0;
	state->parent_action = 0;
	state->cur_obj = NULL;
	state->obj_end = 1;
	state->cur_line = 1;
	if (!(scene->objs))
		obj_array_init(scene);
}

static int	do_loop(t_state *state, t_scene *scene, char *line)
{
	int err;

	if (!(err = line_is_commented(line)))
		if ((err = check_line(line)) < 1)
			parse_error(-err, state->cur_line, line);
	if (err && line[0] != 0 && !line_is_commented(line))
	{
		state->cur_action = start_analysis(line, state->cur_line == 1, state);
		exec_action(state, scene, line);
	}
	else
	{
		ft_memdel((void **)&line);
		return (0);
	}
	return (1);
}

void		get_scene(char *cfile, t_scene *scene)
{
	int		fd;
	char	*line;
	t_state	state;
	int		ret;

	init_loop(&state, scene);
	line = NULL;
	if ((fd = open(cfile, O_RDONLY)) < 0)
		exit(1);
	while (get_next_line(fd, &line) > 0)
	{
		ret = do_loop(&state, scene, line);
		if (ret == 0)
			continue ;
		ft_memdel((void **)&line);
		state.cur_line += 1;
		if (state.cur_action == 5 && state.obj_action == 0)
			break ;
	}
	ft_memdel((void **)&line);
	if (close(fd))
		exit(1);
}
