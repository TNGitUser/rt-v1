/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbaccon <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/20 13:58:13 by qbaccon           #+#    #+#             */
/*   Updated: 2019/07/05 09:29:01 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt_v1.h"

#include <stdio.h>

void	debug_print_vector(t_vector vec)
{
		printf("%10f | %10f | %10f\n", vec.x, vec.y, vec.z);
}


static t_vector     get_vecdir(t_camera cam, t_viewplane vp, float i, float j)
{
	t_vector    res;
	float       xInd;
	float       yInd;

	xInd = vp.width / (float)640;
	yInd = vp.height / (float)480;
	res.x = (vp.ul.x + cam.right.x * xInd * j - cam.up.x * yInd * i) - cam.pos.x;
	res.y = (vp.ul.y + cam.right.y * xInd * j - cam.up.y * yInd * i) - cam.pos.y;
	res.z = (vp.ul.z + cam.right.z * xInd * j - cam.up.z * yInd * i) - cam.pos.z;
	return (res);
}

static int draw_sphere(t_vector cstart, t_vector cdir,
		t_sphere sb, t_inter *inter)
{
	float      dlt;
	float      t1;
	float      t2;
	float      b;
	float      c;
	t_vector   ray;

	/*printf("Printing sphere\n");
	printf("\033[33mCstart\033[0m : ");
	debug_print_vector(cstart);
	printf("\033[33mSb org\033[0m : ");
	debug_print_vector(sb.org);*/
	ray.x = cstart.x - sb.org.x;
	ray.y = cstart.y - sb.org.y;
	ray.z = cstart.z - sb.org.z;/*
	printf("\033[33mRay Sphere\033[0m : ");
	debug_print_vector(ray);*/
	b = cdir.x * ray.x + cdir.y * ray.y + cdir.z * ray.z;
	c = (ray.x * ray.x + ray.y * ray.y + ray.z * ray.z) - sb.rad * sb.rad;
	dlt = ((b * b) - c);
	if (dlt < 0)
		return (0);
	if (inter)
	{	
		if (dlt > 0)
		{
			dlt = (float)sqrt(dlt);
			t1 = (-b + dlt);
			t2 = (-b - dlt);
			if (t1 < 0 || t2 < 0)
				return (0);
			if (t2 < t1)
				t1 = t2;
		}
		else
			t1 = -b;
		inter->vec.x = cstart.x + cdir.x * t1;
		inter->vec.y = cstart.y + cdir.y * t1;
		inter->vec.z = cstart.z + cdir.z * t1;
		inter->norm.x = (inter->vec.x - sb.org.x) / sb.rad;
		inter->norm.y = (inter->vec.y - sb.org.y) / sb.rad;
		inter->norm.z = (inter->vec.z - sb.org.z) / sb.rad;
	}
	return (1);
}

static int	draw_plane(t_vector cstart, t_vector cdir,
		t_plane pl, t_inter *inter)
{
	float		dv;
	t_vector	tmp;
	float		t;

	dv = cdir.x * pl.norm.x + cdir.y * pl.norm.y + cdir.z * pl.norm.z;
	if (dv == 0)
		return (0);
	tmp.x = cstart.x - pl.pos.x;
	tmp.y = cstart.y - pl.pos.y;
	tmp.z = cstart.z - pl.pos.z;
	t = ((-(tmp.x * pl.norm.x + tmp.y * pl.norm.y + tmp.z * pl.norm.z)) / dv);
	if (t < 0)
		return (0);
	if (inter)
	{
		inter->vec.x = cstart.x + cdir.x * t;
		inter->vec.y = cstart.y + cdir.y * t;
		inter->vec.z = cstart.z + cdir.z * t;
		if (dv < 0)
			inter->norm = pl.norm;
		else
		{
			inter->norm.x = -(pl.norm.x);
			inter->norm.y = -(pl.norm.y);
			inter->norm.z = -(pl.norm.z);
		}
	}
	return (1);
}

static void		normalizeC(t_color *c)
{
	if (c->r > 1)
		c->r = 1;
	if (c->b > 1)
		c->b = 1;
	if (c->g > 1)
		c->g = 1;
	if (c->r < 0)
		c->r = 0;
	if (c->b < 0)
		c->b = 0;
	if (c->g < 0)
		c->g = 0;
	c->r = (int)(c->r * 255); 
	c->g = (int)(c->g * 255);
	c->b = (int)(c->b * 255);
	c->fc = ((c->r * 65536) + (c->g * 256) + c->b);
}

static void     normalize(t_vector *vec)
{
	float mag;

	mag = sqrt(vec->x * vec->x + vec->y * vec->y + vec->z * vec->z);
	vec->x = vec->x / mag;
	vec->y = vec->y / mag;
	vec->z = vec->z / mag;
}

static float	getDist(t_inter inter, t_vector cstart)
{
	t_vector tmp;

	tmp.x = inter.vec.x - cstart.x;
	tmp.y = inter.vec.y - cstart.y;
	tmp.z = inter.vec.z - cstart.z;
	return (tmp.x * tmp.x + tmp.y * tmp.y + tmp.z * tmp.z);
}

static t_color	getCLight(t_inter inter, t_vector lpos, t_light l)
{
	t_vector	lv;
	t_color		rc;
	float		angle;

	lv.x = inter.vec.x - lpos.x;
	lv.y = inter.vec.y - lpos.y;
	lv.z = inter.vec.z - lpos.z;
	normalize(&lv);
	angle = inter.norm.x * (-lv.x) + inter.norm.y * (-lv.y) + inter.norm.z * (-lv.z);
	if (angle <= 0)
	{
		rc.r = 0;
		rc.b = 0;
		rc.g = 0;
		return (rc);
	}
	else
	{
		rc.r = l.cdiff * l.color.r * angle;
		rc.g = l.cdiff * l.color.g * angle;
		rc.b = l.cdiff * l.color.b * angle;
		return (rc);
	}
}

static void		getColor(t_light l, t_nod nod, t_color *c, char type,
		t_inter inter)
{
	t_vector 	lv;
	float		lod;
	float		lid;
	t_ray		lray;
	t_inter		li;
	t_vector	tmp;
	int			lb = 0;

	lv.x = inter.vec.x - l.pos.x;
	lv.y = inter.vec.y - l.pos.y;
	lv.z = inter.vec.z - l.pos.z;
	lod = sqrt(lv.x * lv.x + lv.y * lv.y + lv.z * lv.z);
	normalize(&lv);
	lray.start = l.pos;
	lray.dir = lv;
	if (type == 's')
	{
		if (draw_plane(lray.start, lray.dir, nod.pl, &li))
		{
			tmp.x = li.vec.x - l.pos.x;
			tmp.y = li.vec.y - l.pos.y;
			tmp.z = li.vec.z - l.pos.z;
			lid = sqrt(tmp.x * tmp.x + tmp.y * tmp.y + tmp.z * tmp.z);
			if (lid < lod)
				lb = 1;
		}
	}
	if (type == 'p')
	{	
		if (draw_sphere(lray.start, lray.dir, nod.sb, &li))
		{
			tmp.x = li.vec.x - l.pos.x;
			tmp.y = li.vec.y - l.pos.y;
			tmp.z = li.vec.z - l.pos.z;
			lid = sqrt(tmp.x * tmp.x + tmp.y * tmp.y + tmp.z * tmp.z);
			if (lid < lod)
				lb = 1;
		}
	}
	if (lb == 0)
		*c = getCLight(inter, l.pos, l);
}

static t_color	traycer(t_vector cstart, t_vector cdir, t_nod nod)
{
	t_color		c;
	t_inter		ninter;
	t_inter		tmpI;
	char		type = 'v';
	float		tmpD;
	float		dist = 999999;

	c.r = 0;
	c.b = 0;
	c.g = 0;
	c.fc = 0;
	/*printf("\033[32mTRAYCER\033[0m - \n");
	debug_print_vector(cstart);
	debug_print_vector(cdir);
	*/if (draw_sphere(cstart, cdir, nod.sb, &ninter))
	{
		tmpD = getDist(ninter, cstart);
		if (tmpD < dist)
		{
			dist = tmpD;
			tmpI = ninter;
			type = 's';
		}
	}
	if (draw_plane(cstart, cdir, nod.pl, &ninter))
	{
		tmpD = getDist(ninter, cstart);
		if (tmpD < dist)
		{
			dist = tmpD;
			tmpI = ninter;
			type = 'p';
		}
	}
	//getColor(nod.lr, nod, &c, type, tmpI);
	getColor(nod.lb, nod, &c, type, tmpI);
	normalizeC(&c);
	if (type == 'v')
		c.fc = 0;
	return (c);
}

static void	drawer (t_camera cam, t_viewplane vp, t_mlx *mlx, t_nod nod)
{
	t_vector    cdir;
	t_vector    cstart;
	int         i;
	int         j;
	t_color		c;

	j = 0;
	while (j < 640)
	{
		i = 0;
		while (i < 480)
		{

			cstart = cam.pos;
			//printf("DRAWER - %f | %f | %f\n", cstart.x, cstart.y, cstart.z);
			cdir = get_vecdir(cam, vp, i, j);
			normalize(&cdir);
			//printf("DDIR - %f | %f | %f\n", cdir.x, cdir.y, cdir.z);
			c = traycer(cstart, cdir, nod);
			mlx->pctr.data[640 * i + j] = c.fc;
			i++;
		}
		j++;
	}
	mlx_put_image_to_window(mlx->load, mlx->win, mlx->pctr.adr, 0, 0);
}

void get_VPul(t_camera cam, t_viewplane *vp)
{
	vp->ul.x = cam.pos.x + ((cam.dir.x * vp->dist) + (cam.up.x * (vp->height / (float)2)))
		- (cam.right.x * (vp->width / (float)2));
	vp->ul.y = cam.pos.y + ((cam.dir.y * vp->dist) + (cam.up.y * (vp->height / (float)2)))
		- (cam.right.y * (vp->width / (float)2));
	vp->ul.z = cam.pos.z + ((cam.dir.z * vp->dist) + (cam.up.z * (vp->height / (float)2)))
		- (cam.right.z * (vp->width / (float)2));
}

void ini_camera_vp(t_camera *cam, t_viewplane *vp)
{
	cam->dir.x = cam->pov.x - cam->pos.x;
	cam->dir.y = cam->pov.y - cam->pos.y;
	cam->dir.z = cam->pov.z - cam->pos.z;
	normalize(&cam->dir);
	cam->right.x = cam->up.y * cam->dir.z - cam->up.z * cam->dir.y;
	cam->right.y = cam->up.z * cam->dir.x - cam->up.x * cam->dir.z;
	cam->right.z = cam->up.x * cam->dir.y - cam->up.y * cam->dir.x;
	cam->up.x = cam->dir.y * cam->right.z - cam->dir.z * cam->right.y;
	cam->up.y = cam->dir.z * cam->right.x - cam->dir.x * cam->right.z;
	cam->up.z = cam->dir.x * cam->right.y - cam->dir.y * cam->right.x;
	vp->width = 0.5;
	vp->height = 0.35;
	vp->dist = 1;
	get_VPul(*cam, vp);
}

int		deal_key(int key, t_mlx *param)
{
	if (key == 53)
	{
		mlx_destroy_image(param->load, param->pctr.adr);
		mlx_destroy_window(param->load, param->win);
		exit(1);
	}
	return (key);
}
void	debug_cam(t_camera cam)
{
		debug_print_vector(cam.pos);
			debug_print_vector(cam.pov);
				debug_print_vector(cam.dir);
					debug_print_vector(cam.up);
						debug_print_vector(cam.right);
}

int main()
{
	t_sphere    sb;
	t_plane     pl;
	t_mlx       mlx;
	t_camera    cam;
	t_viewplane vp;
	t_light     lr;
	t_light     lb;
	t_nod		nod;

	cam.pos.x = 0;
	cam.pos.y = 3;
	cam.pos.z = -15;
	cam.pov.x = 0;
	cam.pov.y = 0;
	cam.pov.z = 10;
	cam.up.x = 0;
	cam.up.y = 1;
	cam.up.z = 0;
	sb.rad = 2;
	sb.org.x = 2;
	sb.org.y = 1;
	sb.org.z = 20;
	pl.pos.x = 0;
	pl.pos.y = -2;
	pl.pos.z = 0;
	pl.norm.x = 0;
	pl.norm.y = 1;
	pl.norm.z = 0;
	lr.pos.x = 3;
	lr.pos.y = 3;
	lr.pos.z = 0;
	lr.cdiff = 1;
	lr.cspec = 1;
	lr.color.r = 1;
	lr.color.g = 0;
	lr.color.b = 0;
	lb.pos.x = -3;
	lb.pos.y = 3;
	lb.pos.z = 0;
	lb.cdiff = 1;
	lb.cspec = 1;
	lb.color.r = 1;
	lb.color.g = 0;
	lb.color.b = 0;
	nod.sb = sb;
	nod.pl = pl;
	nod.lb = lb;
	nod.lr = lr;
	ini_camera_vp(&cam, &vp);
	normalize(&pl.norm);

	debug_cam(cam);
	mlx.load = mlx_init();
	mlx.win = mlx_new_window(mlx.load, 640, 480, "rt_v1");
	mlx.pctr.adr = mlx_new_image(mlx.load, 640, 480);
	mlx.pctr.data = (int *)mlx_get_data_addr(mlx.pctr.adr, &mlx.pctr.bpp
			, &mlx.pctr.size_l, &mlx.pctr.endian);
	drawer(cam, vp, &mlx, nod);
	mlx_hook(mlx.win, 2, 0, deal_key, &mlx);
	mlx_loop(mlx.load);
}
