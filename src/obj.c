/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   obj.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/27 10:46:54 by lucmarti          #+#    #+#             */
/*   Updated: 2019/07/03 15:37:33 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void		obj_set_type(char *name, t_obj *obj)
{
	ft_memdel((void **)&(obj->name));
	obj->name = ft_strdup(name);
	obj->type = -1;
	if (ft_strcmp(name, "sphere") == 0)
		obj->type = 1;
	else if (ft_strcmp(name, "plane") == 0)
		obj->type = 0;
	else if (ft_strcmp(name, "cone") == 0)
		obj->type = 3;
	else if (ft_strcmp(name, "cylinder") == 0)
		obj->type = 2;
	else if (ft_strcmp(name, "light") == 0)
		obj->type = 4;
}

void		obj_set_color(int color, t_obj *obj)
{
	obj->color.r = ((color >> 16) & 0xFF) / 255.;
	obj->color.g = ((color >> 8) & 0xFF) / 255.;
	obj->color.b = ((color) & 0xFF) / 255.;
}

void		obj_set_pos(t_vector3 pos, t_obj *obj)
{
	obj->pos.x = pos.x;
	obj->pos.y = pos.y;
	obj->pos.z = pos.z;
}

t_obj		*obj_create(char *name, int radius, int height, int length)
{
	t_obj	*obj;

	if (!(obj = malloc(sizeof(t_obj))))
		exit(1);
	obj->name = ft_strdup(name);
	obj_set_type(name, obj);
	obj->radius = radius;
	obj->length = length;
	obj->height = height;
	obj->norm.x = 0;
	obj->norm.y = 0;
	obj->norm.z = 0;
	obj->color.r = 0;
	obj->color.g = 0;
	obj->color.b = 0;
	obj->pos.x = 0;
	obj->pos.y = 0;
	obj->pos.z = 0;
	obj->rot.x = 0;
	obj->rot.y = 0;
	obj->rot.z = 0;
	return (obj);
}

void		obj_free(t_obj *obj)
{
	if (!obj)
		return ;
	ft_memdel((void **)&(obj->name));
	ft_memdel((void **)&obj);
}
