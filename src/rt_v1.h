/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_v1.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbaccon <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/20 15:11:26 by qbaccon           #+#    #+#             */
/*   Updated: 2019/05/29 11:24:28 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RT_V1_H
# define RT_V1_H
# include "../libs/mlx/mlx.h"
# include <math.h>
# include "../libs/libft/libft.h"

// A GARDER
typedef struct  s_pctr 
{
    void    	*adr;
    int			*data;
    int			size_l;
    int			bpp;
    int			endian;
}               t_pctr;

typedef struct  s_vector
{
    float   x;
    float   y;
    float   z;
}               t_vector;

// A GARDER
// RAJOUTER CHAMP POUR TYPE
typedef struct	s_inter
{
	t_vector	vec;
	t_vector	norm;
}				t_inter;


// A GARDER
typedef struct  s_ray
{
    t_vector	start;
	t_vector	dir;
}               t_ray;

typedef struct  s_sphere
{
    t_vector    org;
    float       rad;
	char		type;
}               t_sphere;

typedef struct  s_plane
{
    t_vector    pos;
    t_vector    norm;
	char		type;
}               t_plane;

typedef struct  s_camera
{
    t_vector    pos;
    t_vector    pov;
    t_vector    up;
    t_vector    dir;
    t_vector    right;
}               t_camera;

// A GARDER
typedef struct  s_viewplane
{
    float       width;
    float       height;
    float       dist;
    t_vector    ul;
}               t_viewplane;

typedef struct	s_color
{
	float		r;
	float		g;
	float		b;
	int			fc;
}				t_color;


typedef struct  s_light
{
    t_vector    pos;
    float       cdiff;
    float       cspec;
    t_color		color;
}               t_light;

typedef struct  s_nod
{
    t_list		*o;
	t_list		*l;
	t_sphere	sb;
	t_plane		pl;
	t_light		lr;
	t_light		lb;
}               t_nod;

typedef struct  s_mlx
{
    void        *load;
    void        *win;
    t_pctr      pctr;
}               t_mlx;

#endif
