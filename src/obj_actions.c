/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   obj_actions.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/27 12:39:34 by lucmarti          #+#    #+#             */
/*   Updated: 2019/07/05 09:39:38 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void		obj_verify_correct(t_obj *obj)
{
	if (obj->type == -1)
	{
		if (obj->length != 0 && obj->height != 0 && obj->radius)
			obj->type = 2;
		else if (obj->radius && obj->height != 0 && obj->length == 0)
			obj->type = 3;
		else if (obj->length != 0 || obj->height != 0)
			obj->type = 0;
		else
			obj->type = 1;
	}
	if (obj->length < 0)
		obj->length = -obj->length;
	if (obj->radius < 0)
		obj->radius = -obj->radius;
	if (obj->height < 0)
		obj->height = -obj->height;
	if (!(obj->name))
		obj->name = ft_strdup("NoName");
}

static void	decompose_vector(char *data, t_vector3 *pos)
{
	double	x;
	double	y;
	double	z;

	x = 0;
	y = 0;
	z = 0;
	x = ft_atod(data);
	while (*data && *data != ' ')
		++data;
	y = ft_atod(data);
	data++;
	while (*data && *data != ' ')
		++data;
	z = ft_atod(data);
	pos->x = x;
	pos->y = y;
	pos->z = z;
}

void		exec_set_vector(t_vector3 *vec, t_state *state, char *data)
{
	if (state->obj_action == 2)
	{
		if (state->cur_action == 0)
			decompose_vector(data, vec);
	}
	if (state->obj_action == 1)
		decompose_vector(data, vec);
}

void		exec_obj_set(t_state *state, char *data)
{
	if (state->cur_action == 1)
		state->cur_obj->radius = ft_atoi(data);
	else if (state->cur_action == 3)
		state->cur_obj->height = ft_atoi(data);
	else if (state->cur_action == 2)
		decompose_vector(data, &(state->cur_obj->norm));
}
