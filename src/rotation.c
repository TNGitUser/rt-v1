/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotation.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/05 12:30:48 by lucmarti          #+#    #+#             */
/*   Updated: 2019/07/05 12:54:30 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"


static void	rot_x(t_vector3 *vec, t_vector3 *rot, int invert)
{
	t_vector3	new_vec;

	new_vec.y = vec->y * cos(invert * (rot->x * M_PI / 180.))
		- vec->z * sin(invert * (rot->x * M_PI / 180.));
	new_vec.z = vec->y * sin(invert * (rot->x * M_PI / 180.))
		+ vec->z * cos(invert * (rot->x * M_PI / 180.));
	vec->y = new_vec.y;
	vec->z = new_vec.z;
}

static void	rot_y(t_vector3 *vec, t_vector3 *rot, int invert)
{
	t_vector3	new_vec;

	new_vec.x = vec->x * cos(invert * (rot->y * M_PI / 180.))
		+ vec->z * sin(invert * (rot->y * M_PI / 180.));
	new_vec.z = -1 * vec->x * sin(invert * (rot->y * M_PI / 180.))
		+ vec->z * cos(invert * (rot->y * M_PI / 180.));
	vec->x = new_vec.x;
	vec->z = new_vec.z;
}

static void	rot_z(t_vector3 *vec, t_vector3 *rot, int invert)
{
	t_vector3	new_vec;

	new_vec.x = vec->x * cos(invert * (rot->z * M_PI / 180.))
		- vec->y * sin(invert * (rot->z * M_PI / 180.));
	new_vec.y = vec->x * sin(invert * (rot->z * M_PI / 180.))
		+ vec->y * cos(invert * (rot->z * M_PI / 180.));
	vec->x = new_vec.x;
	vec->y = new_vec.y;
}

void		all_rot(t_vector3 *vec, t_vector3 *rot, int invert)
{
	if (invert == -1)
	{
		rot_x(vec, rot, invert);
		rot_y(vec, rot, invert);
		rot_z(vec, rot, invert);
	}
	else
	{
		rot_z(vec, rot, invert);
		rot_y(vec, rot, invert);
		rot_x(vec, rot, invert);
	}
}
