/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   obj.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 12:48:13 by lucmarti          #+#    #+#             */
/*   Updated: 2019/06/11 13:31:16 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OBJ_H
# define OBJ_H

# include "../libs/libft/libft.h"

/*
**	The structure s_color is used to decode the color, the color being
**	decomposed in three components.
**		R : Red		(from 0 to 1 (akin to a percentage) or from 0 to 255)
**		G : Green	(from 0 to 1 (akin to a percentage) or from 0 to 255)
**		B : Blue	(from 0 to 1 (akin to a percentage) or from 0 to 255)
*/

typedef	struct	s_color
{
	double			r;
	double			g;
	double			b;
	double			fc;
}				t_color;

/*
**	The structure s_obj is used as a generic base to create the objects in a
**	scene. Each object will have commun component such as it's name, type
**	color and position. The radius/height/length are somewhat special.
**		name	: name given to the object (like sphere / sphere-1 / etc...)
**		type	: type of the object (plan = 0 | sphere = 1 | cylinder = 2
**				| cone = 3 | light = 4)
**		radius	: component determining the radius of a sphere/cone/cylinder
**		length	: component determining the length of a plane
**		height	: component determining the height of a cylinder/cone
**		color	: component determining the color of the object (r/g/b)
**		pos		: component determining the position of the object (x/y/z)
**		rot		: component determining the rotation component of the object
**	---------------------------------------------------------------------------
**	WARNING : the s_obj might also be used for the lights. In that case
**			the use of radius might be used to modify the type of the light
**			(spot, diffuse light, concentrated light, etc...)
*/

typedef struct	s_obj
{
	char		*name;
	int			type;
	int			radius;
	int			height;
	int			length;
	t_vector3	norm;
	t_color		color;
	t_vector3	pos;
	t_vector3	rot;
}				t_obj;

/*
**	The structure s_ray is used as base for the ray. Each ray will have a
**	corresponding starting point (vector3) and a direction (vector3).
**		start	: vector3 corresponding to the start point
**		dir		: vector3 corresponding to the direction of the ray
*/

typedef struct	s_ray
{
	t_vector3	start;
	t_vector3	dir;
}				t_ray;

/*
**	The structure s_viewplane is the screen between the camera and the scene.
**	It's through the viewplane that will view the scene.
**		width	: width of the viewplane
**		height	: height of the viewplane
**		ul		: position of the viewplane
*/

typedef struct	s_viewplane
{
	double		width;
	double		height;
	double		dist;
	t_vector3	ul;
}				t_viewplane;

/*
**	The structure s_inter is the intersection structure. It will be used to
**	compute wether we are intersecting a object or not.
**		vec		: vector3 that intersect
**		norm	: vector3 normalized
**		type	: type of the object (sphere/cone/plane/cylinder...)
*/

typedef struct	s_inter
{
	t_vector3	vec;
	t_vector3	norm;
	int			type;
}				t_inter;

/*
**	We will also need methods to determine which element is being hit by the ray
**	Those methods will enable use to correctly manage light/color.
*/

/*
**	obj.c
*/
void			obj_set_type(char *type, t_obj *obj);
void			obj_set_color(int color, t_obj *obj);
void			obj_set_pos(t_vector3 pos, t_obj *obj);
t_obj			*obj_create(char *name, int radius, int length, int height);
void			obj_free(t_obj *obj);

#endif
