/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbaccon <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 13:06:00 by qbaccon           #+#    #+#             */
/*   Updated: 2019/05/29 11:25:29 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static char		*ft_strjoin_prtc(char *s1, char *s2)
{
	char	*new;
	size_t	len;

	if (!s1 || !s2)
		return (NULL);
	len = ft_strlen(s1) + ft_strlen(s2);
	if (!(new = (char *)malloc(len + 1)))
		return (NULL);
	new = ft_strcpy(new, s1);
	new = ft_strcat(new, s2);
	ft_strdel(&s1);
	return (new);
}

static void		readline(int const fd, char **buff)
{
	int		i;
	int		bool;
	char	tmp[BUFF_SIZE + 1];

	bool = 1;
	while (bool && ((i = read(fd, tmp, BUFF_SIZE)) > 0))
	{
		tmp[i] = '\0';
		*buff = ft_strjoin_prtc(*buff, tmp);
		i = 0;
		while (tmp[i] != '\0' && tmp[i] != '\n')
		{
			i++;
			if (tmp[i] == '\n')
				bool = 0;
		}
	}
}

static int		check_buff_len(char *buff)
{
	int		j;

	j = 0;
	while (buff[j] != '\0' && buff[j] != '\n')
		j++;
	return (j);
}

static int		check_buff_bool(char *buff)
{
	int		j;

	j = 0;
	while (buff[j] != '\0' && buff[j] != '\n')
		j++;
	if (buff[j] == '\n')
		return (1);
	return (0);
}

int				get_next_line(int const fd, char **line)
{
	size_t		i;
	char		*tmp;
	static char	*buff;

	if (!line || fd < 0 || BUFF_SIZE <= 0 || read(fd, NULL, 0))
		return (-1);
	if (!buff && !(buff = (char *)malloc(10240)))
		return (-1);
	readline(fd, &buff);
	if (buff[0] != '\0')
	{
		i = check_buff_len(buff);
		*line = ft_strsub(buff, 0, i);
		if (check_buff_bool(buff))
			tmp = ft_strdup(&buff[i + 1]);
		else
			tmp = ft_strdup(&buff[i]);
		ft_strdel(&buff);
		buff = ft_strdup(tmp);
		ft_strdel(&tmp);
		return (1);
	}
	return (0);
}
