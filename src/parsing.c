/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 13:24:03 by lucmarti          #+#    #+#             */
/*   Updated: 2019/07/03 15:38:22 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

static int	select_cam_action(char *line)
{
	size_t	i;
	char	*(action[7]);

	i = 0;
	action[0] = "origin";
	action[1] = "pov";
	action[2] = "up";
	action[3] = "dir";
	action[4] = "right";
	action[5] = "}";
	action[6] = NULL;
	while (action[i])
	{
		if (!ft_strncmp(line, action[i], ft_strlen(action[i])))
		{
			if (line[ft_strlen(action[i])] != 32
					&& line[ft_strlen(action[i])] != 0)
				return (-1);
			return (i);
		}
		++i;
	}
	return (-1);
}

static int	select_obj_action(char *line)
{
	size_t	i;
	char	*(action[9]);

	i = 0;
	action[0] = "name";
	action[1] = "radius";
	action[2] = "length";
	action[3] = "height";
	action[4] = "color";
	action[5] = "origin";
	action[6] = "rot";
	action[7] = "}";
	action[8] = NULL;
	while (action[i])
	{
		if (!ft_strncmp(line, action[i], ft_strlen(action[i])))
		{
			if (line[ft_strlen(action[i])] != 32
					&& line[ft_strlen(action[i])] != 0)
				return (-1);
			return (i);
		}
		++i;
	}
	return (-1);
}

static int	select_main_action(char *line)
{
	size_t	i;
	char	*(action[7]);

	i = 0;
	action[0] = "scene";
	action[1] = "name";
	action[2] = "camera";
	action[3] = "render";
	action[4] = "object";
	action[5] = "}";
	action[6] = NULL;
	while (action[i])
	{
		if (!ft_strncmp(line, action[i], ft_strlen(action[i])))
		{
			if (line[ft_strlen(action[i])] != 32
					&& line[ft_strlen(action[i])] != 0)
				return (-1);
			return (i);
		}
		++i;
	}
	return (-1);
}

static void	aux_analysis(char *line, size_t *i, int is_start)
{
	if (is_start && (ft_strncmp(line, "scene", 5)))
		parse_start_error();
	while (ft_isspace(line[*i]))
		++(*i);
	if (!is_start && (ft_strncmp(line + *i, "scene", 5) == 0))
		parse_scene_error();
}

int			start_analysis(char *line, int is_start, t_state *state)
{
	size_t	i;
	int		action;

	i = 0;
	action = -1;
	aux_analysis(line, &i, is_start);
	if ((line + i)[0] == 0)
		return (-2);
	if (state->obj_action == 0)
		action = select_main_action(line + i);
	else if (state->obj_action == 1)
		action = select_obj_action(line + i);
	else if (state->obj_action == 2)
		action = select_cam_action(line + i);
	if (action == 4 && state->obj_action == 4)
		parse_obj_error();
	if (action == 4 && state->obj_action == 0)
		state->obj_action = 1;
	else if (action == 2 && state->obj_action == 0)
		state->obj_action = 2;
	if (action == -1)
		parse_action_error();
	return (action);
}
