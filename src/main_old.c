/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 12:21:16 by lucmarti          #+#    #+#             */
/*   Updated: 2019/05/29 11:44:01 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void	debug(t_scene scene)
{
	size_t	i;

	i = 0;
	while (i < scene.objs_count)
	{
		printf("\tObject name : [%10s]\n", scene.objs[i]->name);
		printf("\t\ttype   : [%10i]\n", scene.objs[i]->type);
		printf("\t\tradius : [%10i]\n", scene.objs[i]->radius);
		printf("\t\tlength : [%10i]\n", scene.objs[i]->length);
		printf("\t\theight : [%10i]\n", scene.objs[i]->height);
		printf("\t\tcolor  : [%10i]\n", (scene.objs[i]->color.r << 16)
				+ (scene.objs[i]->color.g << 8) + scene.objs[i]->color.b);
		printf("\t\tpos    : [%f,%f,%f]\n", scene.objs[i]->pos.x,
				scene.objs[i]->pos.y, scene.objs[i]->pos.z);
		printf("\t\trot    : [%f,%f,%f]\n", scene.objs[i]->rot.x,
				scene.objs[i]->rot.y, scene.objs[i]->rot.z);
		printf("------\n");
		obj_free(scene.objs[i]);
		++i;
	}
}

int		main(int ac, char **av)
{
	t_scene	scene;
	char	*file;

	scene.objs = NULL;
	scene.name = NULL;
	if (ac == 1)
		file = "./scenes/demo.default";
	else
		file = av[1];
	get_scene(file, &scene);
	printf("Name of the scene : [%10s]\n", scene.name);
	printf("Render settings	  : [%ix%i]\n", scene.render.width,
			scene.render.height);
	printf("Camera settings   -\n");
	printf("\tOrigin    : [%f,%f,%f]\n", scene.camera.pos.x,
			scene.camera.pos.y, scene.camera.pos.z);
	printf("Number of objects : [%10zu]\n", scene.objs_count);
	debug(scene);
	ft_memdel((void **)&(scene.objs));
	ft_memdel((void **)&(scene.name));
	ft_memdel((void **)&(scene));
}
