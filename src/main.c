/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qbaccon <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/20 13:58:13 by qbaccon           #+#    #+#             */
/*   Updated: 2019/07/29 09:35:31 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void	debug_print_vector(t_vector3 vec)
{
	printf("%10f | %10f | %10f\n", vec.x, vec.y, vec.z);
}

static t_vector3     get_vecdir(t_camera cam, t_viewplane vp, float i, float j)
{
	t_vector3    res;
	float       xInd;
	float       yInd;

	xInd = vp.width / (float)640;
	yInd = vp.height / (float)480;
	res.x = (vp.ul.x + cam.right.x * xInd * j - cam.up.x * yInd * i) - cam.pos.x;
	res.y = (vp.ul.y + cam.right.y * xInd * j - cam.up.y * yInd * i) - cam.pos.y;
	res.z = (vp.ul.z + cam.right.z * xInd * j - cam.up.z * yInd * i) - cam.pos.z;
	return (res);
}

static double	dpow(long double n, int pow)
{
	if (n == 0)
		return (0);
	if (pow == 0)
		return (1);
	return (dpow(n, pow - 1) * n);
}

static int	draw_cone(t_ray cray, t_obj *cn, t_inter *inter)
{
	t_vector3	ray;
	double		e[3];
	double		delta;
	double		r[2];

	// Ray should be the difference between the screen pixel and the obj pos
	ray.x = cray.start.x - cn->pos.x;
	ray.y = cray.start.y - cn->pos.y;
	ray.z = cray.start.z - cn->pos.z;

	// e[0] is the x (a) insected, e[1] the y (b), and e[2] the z (c)
	e[0] = dpow(cray.dir.x, 2) + dpow(cray.dir.y, 2) + dpow(cray.dir.z, 2);
	e[1] = 2 * (ray.x * cray.dir.x + ray.y * cray.dir.y
			- ray.z * (cray.dir.z));
	e[2] = dpow(ray.x, 2) + dpow(ray.y, 2)
		- dpow(ray.z, 2);
	delta = e[1] * e[1] - 4 * e[0] * e[2];
	if (delta < 0)
		return (0);
	if (inter)
	{
		delta = (float)sqrt(delta);
		r[0] = (-e[1] + delta) / (2 * e[0]);
		r[1] = (-e[1] - delta) / (2 * e[0]);
		if (r[0] == 0 || r[1] == 0)
			return (0);
		if (r[1] < r[0])
			r[0] = r[1];
		inter->vec.x = cray.start.x + cray.dir.x * r[0];
		inter->vec.y = cray.start.y + cray.dir.y * r[0];
		inter->vec.z = cray.start.z + cray.dir.z * r[0];
		inter->norm.x = (inter->vec.x - cn->pos.x) / cn->radius;
		inter->norm.y = (inter->vec.y - cn->pos.y) / cn->radius;
		inter->norm.z = (inter->vec.z - cn->pos.z) / cn->radius;
	}
	return (1);
}

static int	draw_cylinder(t_ray cray, t_obj *cn, t_inter *inter)
{
	t_vector3	ray;
	double		e[3];
	double		delta;
	double		r[2];
	double		y[2];
	double		i;
	int			flag;

	if (cn->height == 0)
		return (0);
	flag = 0;
	i = 0;
	// Ray should be the difference between the screen pixel and the obj pos
	ray.x = cray.start.x - cn->pos.x;
	ray.y = cray.start.y - cn->pos.y;
	ray.z = cray.start.z - cn->pos.z;

	// e[0] is the x (a) insected, e[1] the y (b), and e[2] the z (c)
	e[0] = dpow(cray.dir.x, 2) + dpow(cray.dir.z, 2);
	e[1] = 2 * ray.x * cray.dir.x + 2 * ray.z * cray.dir.z;
	e[2] = dpow(ray.x, 2) + dpow(ray.z, 2) - dpow(cn->radius, 2);
	delta = e[1] * e[1] - 4 * e[0] * e[2];
	if (delta < 0)
		return (0);
	if (delta == 0)
		r[0] = -e[1] / (2 * e[0]);
	else
	{
		delta = (double)sqrt(delta);
		r[0] = (-e[1] + delta) / (2 * e[0]);
		r[1] = (-e[1] - delta) / (2 * e[0]);
		if (r[0] == 0 || r[1] == 0)
			return (0);
		if (r[1] < r[0] && r[1] < delta)
			r[0] = r[1];
		y[0] = ray.y + r[0] * cray.dir.y;
		y[1] = ray.y + r[1] * cray.dir.y;
		if ((ray.y > 0 || cray.start.y < 0) && ray.x * ray.x + ray.z * ray.z <= cn->height * cn->height)
		{
			i = - ray.y / (cray.dir.y == 0 ? 1 : cray.dir.y);
			inter->vec.y = cray.start.y + cray.dir.y * i;
			inter->vec.z = cray.start.z + cray.dir.z * i;
			inter->vec.x = cray.start.x + cray.dir.x * i;
			if (!(i > delta && dpow(inter->vec.z, 2) + dpow(inter->vec.x, 2) <= cn->height * cn->height))
				return (0);
		}
		else if ((ray.y > cn->pos.y + cn->height || cray.start.y > cn->pos.y + cn->height) && ray.x * ray.x + ray.z * ray.z <= cn->height * cn->height)
		{
			i = ((cn->pos.y + (double)cn->height) - ray.y) / (cray.dir.y == 0 ? 1 : cray.dir.y);
			inter->vec.y = cray.start.y + cray.dir.y * i;
			inter->vec.z = cray.start.z + cray.dir.z * i;
			inter->vec.x = cray.start.x + cray.dir.x * i;
			if (!(i > delta && dpow(inter->vec.z, 2) + dpow(inter->vec.x, 2) <= cn->height * cn->height))
				return (0);
		}
		else
			i = r[0];
		/*if (cn->pos.y < y[0] && y[0] < cn->pos.y + (double)cn->height)
		{	
			flag = 1;
			i = y[0];
		}
		if (cn->pos.y < y[1] && y[1] < cn->pos.y + (double)cn->height)
			i = flag && y[0] < y[1] && y[0] >= 0. ? i : y[1]; 
		if (i == y[0])
			i = (y[0] - ray.y) / (cray.dir.y == 0 ? 1 : cray.dir.y);
		if (i == y[1])
			i = (y[1] - ray.y) / (cray.dir.y == 0 ? 1 : cray.dir.y);*/
	}
	if (cn->height == 0 || i <= 0)
		return (0);
	inter->vec.x = cray.start.x + cray.dir.x * i;//r[0];
	inter->vec.y = cray.start.y + cray.dir.y * i;//r[0];
	inter->vec.z = cray.start.z + cray.dir.z * i;//r[0];
	inter->norm.x = (inter->vec.x - cn->pos.x) / cn->radius;
	inter->norm.y = (inter->vec.y - cn->pos.y) / cn->radius;
	inter->norm.z = (inter->vec.z - cn->pos.z) / cn->radius;
	return (1);
}

static int draw_sphere(t_ray cray, t_obj *sb, t_inter *inter)
{
	float      dlt;
	float      t1;
	float      t2;
	float      b;
	float      c;
	t_vector3   ray;

	ray.x = cray.start.x - sb->pos.x;
	ray.y = cray.start.y - sb->pos.y;
	ray.z = cray.start.z - sb->pos.z;
	b = cray.dir.x * ray.x + cray.dir.y * ray.y + cray.dir.z * ray.z;
	c = (ray.x * ray.x + ray.y * ray.y + ray.z * ray.z) - dpow(sb->radius, 2);
	dlt = ((b * b) - c);
	if (dlt < 0)
		return (0);
	if (inter)
	{	
		if (dlt > 0)
		{
			dlt = (float)sqrt(dlt);
			t1 = (-b + dlt);
			t2 = (-b - dlt);
			if (t1 < 0 || t2 < 0)
				return (0);
			if (t2 < t1)
				t1 = t2;
		}
		else
			t1 = -b;
		inter->vec.x = cray.start.x + cray.dir.x * t1;
		inter->vec.y = cray.start.y + cray.dir.y * t1;
		inter->vec.z = cray.start.z + cray.dir.z * t1;
		inter->norm.x = (inter->vec.x - sb->pos.x) / sb->radius;
		inter->norm.y = (inter->vec.y - sb->pos.y) / sb->radius;
		inter->norm.z = (inter->vec.z - sb->pos.z) / sb->radius;
	}
	return (1);
}

static int	draw_plane(t_ray cray, t_obj *pl, t_inter *inter)
{
	float		dv;
	t_vector3	tmp;
	float		t;

	dv = cray.dir.x * pl->norm.x + cray.dir.y * pl->norm.y + cray.dir.z * pl->norm.z;
	if (dv == 0)
		return (0);
	tmp.x = cray.start.x - pl->pos.x;
	tmp.y = cray.start.y - pl->pos.y;
	tmp.z = cray.start.z - pl->pos.z;
	t = ((-(tmp.x * pl->norm.x + tmp.y * pl->norm.y + tmp.z * pl->norm.z)) / dv);
	if (t < 0)
		return (0);
	if (inter)
	{
		inter->vec.x = cray.start.x + cray.dir.x * t;
		inter->vec.y = cray.start.y + cray.dir.y * t;
		inter->vec.z = cray.start.z + cray.dir.z * t;
		if (dv < 0)
			inter->norm = pl->norm;
		else
		{
			inter->norm.x = -(pl->norm.x);
			inter->norm.y = -(pl->norm.y);
			inter->norm.z = -(pl->norm.z);
		}
	}
	return (1);
}

static void		normalizeC(t_color *c)
{
	if (c->r > 1)
		c->r = 1;
	if (c->b > 1)
		c->b = 1;
	if (c->g > 1)
		c->g = 1;
	if (c->r < 0)
		c->r = 0;
	if (c->b < 0)
		c->b = 0;
	if (c->g < 0)
		c->g = 0;
	c->r = (int)(c->r * 255); 
	c->g = (int)(c->g * 255);
	c->b = (int)(c->b * 255);
	c->fc = ((c->r * 65536) + (c->g * 256) + c->b);
}

static void     normalize(t_vector3 *vec)
{
	float mag;

	mag = sqrt(vec->x * vec->x + vec->y * vec->y + vec->z * vec->z);
	vec->x = vec->x / mag;
	vec->y = vec->y / mag;
	vec->z = vec->z / mag;
}

static float	getDist(t_inter inter, t_vector3 cstart)
{
	t_vector3 tmp;

	tmp.x = inter.vec.x - cstart.x;
	tmp.y = inter.vec.y - cstart.y;
	tmp.z = inter.vec.z - cstart.z;
	return (tmp.x * tmp.x + tmp.y * tmp.y + tmp.z * tmp.z);
}

static t_color	getCLight(t_inter inter, t_vector3 lpos, t_color co)
{
	t_vector3	lv;
	t_color		rc;
	float		angle;

	lv.x = inter.vec.x - lpos.x;
	lv.y = inter.vec.y - lpos.y;
	lv.z = inter.vec.z - lpos.z;
	normalize(&lv);
	angle = inter.norm.x * (-lv.x) + inter.norm.y * (-lv.y) + inter.norm.z * (-lv.z);
	if (angle <= 0)
	{
		rc.r = 0;
		rc.b = 0;
		rc.g = 0;
		return (rc);
	}
	else
	{
		rc.r = /*l.cdiff * */ co.r * angle;
		rc.g = /*l.cdiff * */ co.g * angle;
		rc.b = /*l.cdiff * */ co.b * angle;
		return (rc);
	}
}

static void		get_color(t_scene sc, t_inter inter, t_obj *cO, t_color *c)
{
	t_vector3 	lv;
	float		lod;
	float		lid;
	t_ray		lray;
	t_inter		li;
	t_vector3	tmp;
	int			lb = 0;
	size_t		i = 0;
	size_t		j = 0;
	t_obj		*tmpO;


	while (i < sc.objs_count)
	{
		lb = 0;
		if (sc.objs[i]->type == 4)
		{
			lv.x = inter.vec.x - sc.objs[i]->pos.x;
			lv.y = inter.vec.y - sc.objs[i]->pos.y;
			lv.z = inter.vec.z - sc.objs[i]->pos.z;
			lod = sqrt(lv.x * lv.x + lv.y * lv.y + lv.z * lv.z);
			normalize(&lv);
			lray.start = sc.objs[i]->pos;
			lray.dir = lv;
			j = 0;
			while (j < sc.objs_count)
			{
				tmpO = sc.objs[j];
				if (tmpO != cO)
				{
					if (tmpO->type == 1)
						if (draw_sphere(lray, sc.objs[j], &li))
						{
							tmp.x = li.vec.x - sc.objs[i]->pos.x;
							tmp.y = li.vec.y - sc.objs[i]->pos.y;
							tmp.z = li.vec.z - sc.objs[i]->pos.z;
							lid = sqrt(tmp.x * tmp.x + tmp.y * tmp.y + tmp.z * tmp.z);
							if (lid < lod)
								lb = 1;
						}
					if (tmpO->type == 0)
						if (draw_plane(lray, sc.objs[j], &li))
						{
							tmp.x = li.vec.x - sc.objs[i]->pos.x;
							tmp.y = li.vec.y - sc.objs[i]->pos.y;
							tmp.z = li.vec.z - sc.objs[i]->pos.z;
							lid = sqrt(tmp.x * tmp.x + tmp.y * tmp.y + tmp.z * tmp.z);
							if (lid < lod)
								lb = 1;
						}
					if (tmpO->type == 2)
						if (draw_cylinder(lray, sc.objs[j], &li))
						{
							tmp.x = li.vec.x - sc.objs[i]->pos.x;
							tmp.y = li.vec.y - sc.objs[i]->pos.y;
							tmp.z = li.vec.z - sc.objs[i]->pos.z;
							lid = sqrt(tmp.x * tmp.x + tmp.y * tmp.y + tmp.z * tmp.z);
							if (lid < lod)
								lb = 1;
						}
					if (tmpO->type == 3)
						if (draw_cone(lray, sc.objs[j], &li))
						{
							tmp.x = li.vec.x - sc.objs[i]->pos.x;
							tmp.y = li.vec.y - sc.objs[i]->pos.y;
							tmp.z = li.vec.z - sc.objs[i]->pos.z;
							lid = sqrt(tmp.x * tmp.x + tmp.y * tmp.y + tmp.z * tmp.z);
							if (lid < lod)
								lb = 1;
						}
				}
				j++;
			}
			if (lb == 0)
				*c = getCLight(inter, sc.objs[i]->pos, cO->color);
		}
		i++;
	}
}

static t_color	traycer(t_ray cray, t_scene sc)
{
	t_color		c;
	t_inter		ninter;
	t_inter		tmpI;
	t_obj		*tmpO;
	size_t		i = 0;
	float		tmpD;
	float		dist = 999999;

	c.r = 0;
	c.b = 0;
	c.g = 0;
	c.fc = 0;
	while (i < sc.objs_count)
	{
		if (sc.objs[i]->type == 0 && draw_plane(cray, sc.objs[i], &ninter))
		{
			tmpD = getDist(ninter, cray.start);
			if (tmpD < dist)
			{
				dist = tmpD;
				tmpI = ninter;
				tmpO = sc.objs[i];
			}
		}
		else if (sc.objs[i]->type == 1 && draw_sphere(cray, sc.objs[i], &ninter))
		{
			tmpD = getDist(ninter, cray.start);
			if (tmpD < dist)
			{
				dist = tmpD;
				tmpI = ninter;
				tmpO = sc.objs[i];
			}
		}
		else if (sc.objs[i]->type == 2 && draw_cylinder(cray, sc.objs[i], &ninter))
		{
			tmpD = getDist(ninter, cray.start);
			if (tmpD < dist)
			{
				dist = tmpD;
				tmpI = ninter;
				tmpO = sc.objs[i];
			}
		}
		else if (sc.objs[i]->type == 3 && draw_cone(cray, sc.objs[i], &ninter))
		{
			tmpD = getDist(ninter, cray.start);
			if (tmpD < dist)
			{
				dist = tmpD;
				tmpI = ninter;
				tmpO = sc.objs[i];
			}
		}
		i++;
	}
	if (dist != 999999)
	{
		get_color(sc, tmpI, tmpO, &c);
		normalizeC(&c);
	}
	if (tmpO->type == 4)
		c.fc = 0;
	return (c);
}

static void	drawer (t_scene *sc, t_render *rnd, t_pctr *pctr)
{
	t_ray		cray;
	int         i;
	int         j;
	t_color		c;

	j = 0;
	while (j < 640)
	{
		i = 0;
		while (i < 480)
		{
			cray.start = sc->camera.pos;
			cray.dir = get_vecdir(sc->camera, sc->vp, i, j);
			normalize(&cray.dir);
			c = traycer(cray, *sc);
			pctr->data[640 * i + j] = c.fc;
			i++;
		}
		j++;
	}
	mlx_put_image_to_window(rnd->mlx_ptr, rnd->mlx_win, pctr->adr, 0, 0);
}

void get_VPul(t_camera cam, t_viewplane *vp)
{
	vp->ul.x = cam.pos.x + ((cam.dir.x * vp->dist) + (cam.up.x * (vp->height / (float)2)))
		- (cam.right.x * (vp->width / (float)2));
	vp->ul.y = cam.pos.y + ((cam.dir.y * vp->dist) + (cam.up.y * (vp->height / (float)2)))
		- (cam.right.y * (vp->width / (float)2));
	vp->ul.z = cam.pos.z + ((cam.dir.z * vp->dist) + (cam.up.z * (vp->height / (float)2)))
		- (cam.right.z * (vp->width / (float)2));
}

void ini_camera_vp(t_camera *cam, t_viewplane *vp)
{
	cam->dir.x = cam->pov.x - cam->pos.x;
	cam->dir.y = cam->pov.y - cam->pos.y;
	cam->dir.z = cam->pov.z - cam->pos.z;
	normalize(&cam->dir);
	cam->right.x = cam->up.y * cam->dir.z - cam->up.z * cam->dir.y;
	cam->right.y = cam->up.z * cam->dir.x - cam->up.x * cam->dir.z;
	cam->right.z = cam->up.x * cam->dir.y - cam->up.y * cam->dir.x;
	cam->up.x = cam->dir.y * cam->right.z - cam->dir.z * cam->right.y;
	cam->up.y = cam->dir.z * cam->right.x - cam->dir.x * cam->right.z;
	cam->up.z = cam->dir.x * cam->right.y - cam->dir.y * cam->right.x;
	vp->width = 0.5;
	vp->height = 0.35;
	vp->dist = 1;
	get_VPul(*cam, vp);
}

int		deal_key(int key, void *param)
{
	t_scene *sc = param;
	if (key == 53)
	{
		mlx_destroy_image(sc->render.mlx_ptr, sc->render.pctr.adr);
		mlx_destroy_window(sc->render.mlx_ptr, sc->render.mlx_win);
		exit(1);
	}
	return (key);
}

void	debug(t_scene *scene)
{
	size_t	i;

	i = 0;
	while (i < scene->objs_count)
	{
		printf("\tObject name : [%10s]\n", scene->objs[i]->name);
		printf("\t\ttype   : [%10i]\n", scene->objs[i]->type);
		printf("\t\tradius : [%10i]\n", scene->objs[i]->radius);
		printf("\t\tlength : [%10i]\n", scene->objs[i]->length);
		printf("\t\theight : [%10i]\n", scene->objs[i]->height);
		//	printf("\t\tcolor  : [%10i]\n", (scene->objs[i]->color.r << 16)
		//			+ (scene->objs[i]->color.g << 8) + scene->objs[i]->color.b);
		printf("\t\tcolor R: [%10f]\n", (scene->objs[i]->color.r));
		printf("\t\tcolor G: [%10f]\n", (scene->objs[i]->color.g));
		printf("\t\tcolor B: [%10f]\n", (scene->objs[i]->color.b));
		printf("\t\tpos    : [%f,%f,%f]\n", scene->objs[i]->pos.x,
				scene->objs[i]->pos.y, scene->objs[i]->pos.z);
		printf("\t\trot    : [%f,%f,%f]\n", scene->objs[i]->rot.x,
				scene->objs[i]->rot.y, scene->objs[i]->rot.z);
		all_rot(&(scene->objs[i]->pos), &(scene->objs[i]->rot), 1);
		printf("\t\tRpos   : [%f,%f,%f]\n", scene->objs[i]->pos.x,
				scene->objs[i]->pos.y, scene->objs[i]->pos.z);
		printf("------\n");
		//obj_free(scene->objs[i]);
		++i;
	}
}


void	debug_cam(t_scene scene)
{
	debug_print_vector(scene.camera.pos);
	debug_print_vector(scene.camera.pov);
	debug_print_vector(scene.camera.dir);
	debug_print_vector(scene.camera.up);
	debug_print_vector(scene.camera.right);
}

int main(int ac, char **av)
{
	t_scene		scene;
	t_camera    cam;
	t_viewplane vp;
	char		*file;

	cam.pos.x = 0;
	cam.pos.y = 10;
	cam.pos.z = -100;
	cam.pov.x = 0;
	cam.pov.y = 0;
	cam.pov.z = 0;
	cam.up.x = 0;
	cam.up.y = 1;
	cam.up.z = 0;
	/*sb.rad = 2;
	  sb.org.x = 0;
	  sb.org.y = 0;
	  sb.org.z = 10;
	  pl.pos.x = 0;
	  pl.pos.y = -2;
	  pl.pos.z = 0;
	  pl.norm.x = 0;
	  pl.norm.y = 1;
	  pl.norm.z = 0;
	  cn.org.x = 0;
	  cn.org.y = 0;
	  cn.org.z = 10;
	  cn.hight.x = 0;
	  cn.hight.y = 4;
	  cn.hight.z = 10;
	  cn.rad = 2;
	  lr.pos.x = 3;
	  lr.pos.y = 3;
	  lr.pos.z = 0;
	  lr.cdiff = 1;
	  lr.cspec = 1;
	  lr.color.r = 1;
	  lr.color.g = 0;
	  lr.color.b = 0;
	  lb.pos.x = -3;
	  lb.pos.y = 3;
	  lb.pos.z = 0;
	  lb.cdiff = 1;
	  lb.cspec = 1;
	  lb.color.r = 0;
	  lb.color.g = 0;
	  lb.color.b = 1;*/
	ini_camera_vp(&cam, &vp);
	scene.objs = NULL;
	if (ac == 1)
		file = "./scenes/demo.2";
	else
		file = av[1];
	get_scene(file, &scene);
	scene.vp = vp;
	scene.camera = cam;
	debug_cam(scene);
	printf("Name of the scene : [%10s]\n", scene.name);
	printf("Render settings	  : [%ix%i]\n", scene.render.width,
			scene.render.height);
	printf("Camera settings   -\n");
	printf("\tOrigin    : [%f,%f,%f]\n", scene.camera.pos.x,
			scene.camera.pos.y, scene.camera.pos.z);
	printf("Number of objects : [%10zu]\n", scene.objs_count);
	debug(&scene);
	scene.render.mlx_ptr = mlx_init();
	/*scene.render.mlx_win = mlx_new_window(scene.render.mlx_ptr
	  , scene.render.width, scene.render.height, scene.name);
	  scene.render.pctr.adr = mlx_new_image(scene.render.mlx_ptr
	  , scene.render.width, scene.render.height);*/
	scene.render.mlx_win = mlx_new_window(scene.render.mlx_ptr, 640, 480
			, scene.name);
	scene.render.pctr.adr = mlx_new_image(scene.render.mlx_ptr, 640, 480);
	scene.render.pctr.data = (int *)mlx_get_data_addr(scene.render.pctr.adr
			, &scene.render.pctr.bpp
			, &scene.render.pctr.size_l, &scene.render.pctr.endian);
	drawer(&scene, &(scene.render), &(scene.render.pctr));
	mlx_hook(scene.render.mlx_win, 2, 0, deal_key, &scene);
	mlx_loop(scene.render.mlx_ptr);
}
