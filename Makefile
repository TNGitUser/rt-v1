# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/03/28 15:42:43 by lucmarti          #+#    #+#              #
#    Updated: 2019/07/05 12:34:06 by lucmarti         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

Red=\x1b[31m
Green=\x1b[32m
Cyan=\x1b[36m
Yellow=\x1b[34m
End=\x1b[0m

NAME = rtv1

SRC_PATH = ./src
SRC_FILE = main.c init.c get_next_line.c file.c array.c misc.c parsing_error.c\
		   parsing.c actions.c obj.c error.c obj_actions.c rotation.c

OBJ_PATH = ./objs
OBJ_FILE = $(SRC_FILE:.c=.o)
DEPS_FILE = $(SRC_FILE:.c=.d)

SRC = $(addprefix $(SRC_PATH)/,$(SRC_FILE))
OBJ = $(addprefix $(OBJ_PATH)/,$(OBJ_FILE))
DEPS = $(addprefix $(OBJ_PATH)/,$(DEPS_FILE))

LIB_OBJ = libft.a libmlx.a
LIBS = $(addprefix $(OBJ_PATH)/,$(LIB_OBJ))

CFLAGS = -Wall -Wextra -g3 -I./includes -I./libs/libft -I./libs/mlx
MLXFLAGS = -framework OpenGL -framework AppKit -lm -lpthread
CPPFLAGS = -MMD -MP 

ifndef VERBOSE
.SILENT:
VERBOSE=0
endif

all: $(NAME)

libft: FORCE
	@mkdir $(OBJ_PATH) 2> /dev/null || true
	@$(MAKE) -C ./libs/libft "VERBOSE=$(VERBOSE)"
	@cp -a ./libs/libft/libft.a $(OBJ_PATH)/libft.a

mlx: FORCE
	@mkdir $(OBJ_PATH) 2> /dev/null || true
	@$(MAKE) -C ./libs/mlx "VERBOSE=$(VERBOSE)"
	@cp -a ./libs/mlx/libmlx.a $(OBJ_PATH)/libmlx.a

$(NAME): libft mlx $(OBJ)
	@gcc $(CFLAGS) $(MLXFLAGS) -o $(NAME) $(OBJ) $(LIBS)
	@echo "Compilation of $(Cyan)$(NAME)$(End)   :    ${Green}Done${End}"

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@mkdir $(OBJ_PATH) 2> /dev/null || true
	@gcc $(CFLAGS) $(CPPFLAGS) -o $@ -c $<
	@echo "[${Green}Compiled${End}] : $<"

.PHONY: clean fclean miniclean minire re

clean:
	@rm -f $(OBJ)
	@rm -f $(LIBS)
	@$(RM) $(DEPS)
	@$(MAKE) -C ./libs/libft/ clean 
	@$(MAKE) -C ./libs/mlx/ clean 
	@rmdir $(OBJ_PATH) 2> /dev/null || (true && if [ -d "$(OBJ_PATH)" ]; then\
		echo "$(Red)ERROR$(End)	: $(OBJ_PATH) is not empty."; fi)
	@echo "$(Red)$(NAME)$(End) : Removing objects"

fclean: clean
	@rm -f $(NAME)
	@$(MAKE) -C ./libs/libft/ fclean 
	@echo "$(Red)$(NAME)$(End) : Removing $(NAME)"

miniclean:
	@rm -f $(OBJ)

minire: miniclean all

re: fclean all
-include $(DEPS)
FORCE:
