# How to write a scene for RT_V1

#### The scene

First we need to declare a scene in our file. One file can only have one scene.
**To declare it do** :
`scene {`
The scene must be the first declaration of the file.
After declaring the scene component we might want to add more spice to it, to do this we'll have to use the following components:
- name : Obviously the name of the scene (what the window will be named)
- camera : Camera component of the scene, is an object that has multiples attributes
- render : Render component of the scene, ------------------------------------------
- objects : Objects component of the scene (sphere, plane, cone, cylinder), have attributes

**To end the declaration of the scene write the following alone on the last line **:
`}`

#### Camera object

**Attributes** :
- origin : inital position
- see with Qbaccon for the others (pov, up, right, dir)

**To declare a camera** :
~~~~   
camera {
    origin { X Y Z }
    }
~~~~

#### Render object

**Attributes** :
- Resolution : width and height of the scene

**To declare a render** :
~~~~   
render { WIDTH HEIGHT }
~~~~

#### OBJECT

This component is special because is it the generic component for real object in the scene (light, cone, cylinder, plane, sphere).

**Attributes** :
- name : name of the object, more type in fact (write light, cone, cylinder, plane or sphere. If you do not, type will be defined following the attributes (which might be wrong))
- radius : radius attribute for sphere, cone, cylinder and light, integer is prefered
- length & height : attributes for plane, cylinder, cone, integer is prefered.
- color : vector 3 (red / green / blue) for color, express it in hexadecimal
- origin : initial position in 3d (x, y, z)
- rot : initial rotation in 3d (xr, xy, xz)


**To declare a obj** :
~~~~   
object {
    name { type }
    radius { 100.0 }
    height { 50.0 }
    length { 175.0 }
    color { 0xFF00FF }
    origin { 150. 0. 25. }
    rot { 0. 0. 0.}
}
~~~~
